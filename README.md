# Kali in a container

![image](/uploads/70131b5e1ae91f96e2e49a816ac04287/image.png)

## Requirements

- docker
- docker-compose


## Setup

```bash
# build the image
docker-compose build

# run the container
docker-compose up -d
```


## SSH with display forwarding

You can connect (graphically) to SSH with `-Y` option

```bash
ssh -Y user@localhost -p 4242
```

`user` password is `resu`


## Launch UI apps

You can browse your target :80 with Firefox

```bash
firefox "http://mytarget.lab"
```


## Connect to a VPN

You can connect to your lab
```bash
screen
# [Enter]
sudo openvpn lab.ovpn
# [Ctrl] + [A] + [D]
```


## Root privileges

You can also be root easily (legit Kali needs)
```bash
sudo su
```
