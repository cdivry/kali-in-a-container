FROM kalilinux/kali-rolling

# avoid question prompt
ENV DEBIAN_FRONTEND noninteractive

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# pre-installing x11 and some kali tools
RUN apt update && \
	apt upgrade -y && \
	apt install -y \
	apt-utils \
	locales \
    ca-certificates \
    net-tools \
    sudo \
    supervisor \
    wget \
    openssh-server \
	dbus-x11 \
	x11-apps \
    x11-xserver-utils \
    xauth \
    xfce4 \
    xfce4-terminal \
    xterm \
	git \
	make \
	gdb-peda \
	emacs-nox \
	curl \
	wget \
	gcc \
	screen \
	metasploit-framework \
	dirbuster \
	openvpn \
	bridge-utils \
	iputils-ping \
	firefox-esr \
	python3-pip \
	netcat-traditional \
	php \
	hashcat \
	john \
	sqlmap \
	hydra \
	burpsuite

RUN pip install pwncat

# SSH server configuration

RUN mkdir -p /var/run/sshd
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config && \
    sed -ri 's/^#AllowTcpForwarding\s+.*/AllowTcpForwarding yes/g' /etc/ssh/sshd_config && \
    sed -ri 's/^#X11Forwarding\s+.*/X11Forwarding yes/g' /etc/ssh/sshd_config && \
    sed -ri 's/^#X11UseLocalhost\s+.*/X11UseLocalhost no/g' /etc/ssh/sshd_config


# user:resu creation

RUN useradd -ms /bin/bash --password 6HDJXfMtH6sCY user && \
    adduser user sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers


USER user
WORKDIR /home/user

RUN wget "https://raw.githubusercontent.com/zacheller/rockyou/master/rockyou.txt.tar.gz" && \
	tar xvf rockyou.txt.tar.gz

RUN wget "https://raw.githubusercontent.com/daviddias/node-dirbuster/master/lists/directory-list-2.3-medium.txt"

RUN touch /home/user/.Xauthority
COPY lab.ovpn /home/user/lab.ovpn
RUN sudo chmod 600 /home/user/lab.ovpn


USER root
WORKDIR /root
ENTRYPOINT ["/entrypoint.sh"]
