#!/bin/bash

echo "--- INIT ---"

# starting up SSH server
sudo /usr/sbin/sshd

# wait for pod kill
touch /tmp/.wait.pod
tail -f /tmp/.wait.pod

echo "=== END ==="
