#!/bin/bash

# SPAWN:
# 1. docker-compose build
# 2. docker-compose up -d

# DIRECT ACCESS
#CONTAINER_NAME=$(docker ps --format "table {{.Names}}" | grep etabli)
#docker exec -it "$CONTAINER_NAME" bash

# SSH ACCESS
ssh -Y user@127.0.0.1 -p 4242
